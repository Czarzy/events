# README #

I've updated an RESTful API from Piotr Gankiewicz's course (can be found at https://strefakursow.pl/kursy/web_develop/asp_net_core_-_budowa_aplikacji.html)
from ASP.NET Core 1.1 to ASP.NET Core 2.0. 

In this course author made simple API to manage events and accounts using Visual Studio Code on Linux OS, I've used
Visual Studio 2017 on Windows 10 OS.

I've made it just for educational purpose.