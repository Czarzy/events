﻿using Events.Core.Domain;
using Events.Core.Repositories;
using Events.Infrastructure.Repositories;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Events.TestsProj.Repositories
{
    public class UserRepositoryTests
    {
        [Fact]
        public async Task adding_new_user()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user", "test", "test@test.com", "secret");
            IUserRepository repository = new UserRepository();
            //Act
            await repository.AddAsync(user);
            //Assert
            var existingUser = await repository.GetAsync(user.Id);
            Assert.True(user.Equals(existingUser));
        }
    }
}
