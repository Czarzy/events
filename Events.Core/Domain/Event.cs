﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Events.Core.Domain
{
    public class Event: Entity
    {
        //hashset to add tickets
        private ISet<Ticket> _tickets = new HashSet<Ticket>();

        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public DateTime CreatedAt { get; protected set; }
        public DateTime StartDate { get; protected set; }
        public DateTime EndDate { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }

        //encapsulation so consumer can't add new tickets
        public IEnumerable<Ticket> Tickets => _tickets;

        public IEnumerable<Ticket> PurchasedTickets => _tickets.Where(x => x.Purchased);

        public IEnumerable<Ticket> AvailableTickets => _tickets.Where(x => !x.Purchased);

        protected Event()
        { }

        public Event(Guid id, string name, string description, DateTime startDate, DateTime endDate)
        {
            Id = id;
            SetName(name);
            SetDescription(description);
            CreatedAt = DateTime.UtcNow;
            SetDate(startDate, endDate);
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetDate(DateTime startDate, DateTime endDate)
        {
            if(startDate >= endDate)
            {
                throw new Exception("Start date can't be greater than end date!");
            }
            StartDate = startDate;
            EndDate = endDate;
        }

        public void SetName(string name)
        {
            if(string.IsNullOrWhiteSpace(name))
            {
                throw new Exception($"Event with id: {Id} can't have an empty name.");
            }
            Name = name;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetDescription(string description)
        {
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new Exception($"Event with id: {Id} can't have an empty Description.");
            }
            Description = description;
            UpdatedAt = DateTime.UtcNow;
        }

        public void AddTicket(int amount, decimal price)
        {
            var seating = _tickets.Count + 1;
            for (var i = 0; i<amount; i++)
            {
                _tickets.Add(new Ticket(this, seating, price));
                seating++;
            }
        }

        public void PurchaseTicket(User user, int amount)
        {
            if(AvailableTickets.Count() < amount)
            {
                throw new Exception($"There are only {AvailableTickets} tickets available.");
            }
            var tickets = AvailableTickets.Take(amount);
            foreach(var ticket in tickets)
            {
                ticket.Purchase(user);
            }
        }

        public void CancelPurchasedTickets(User user, int amount)
        {
            var tickets = GetTicketsPurchasedByUser(user);
            if(tickets.Count() < amount)
            {
                throw new Exception($"Not enough purchased tickets to be cancelled: {amount}.");
            }

            foreach(var ticket in tickets.Take(amount))
            {
                ticket.Cancel();
            }
        }

        public IEnumerable<Ticket> GetTicketsPurchasedByUser(User user)
        => PurchasedTickets.Where(x => x.UserId == user.Id);
    }
}
