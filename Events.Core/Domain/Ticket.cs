﻿using System;

namespace Events.Core.Domain
{
    public class Ticket: Entity
    {
        public Guid EventId { get; private set; }

        public int Seating { get; private set; }

        public decimal Price { get; private set; }

        public Guid? UserId{ get; private set; }

        public string Username { get; private set; }

        public DateTime? PurchasedAt { get; private set; }

        public bool Purchased => PurchasedAt.HasValue;

        protected Ticket()
        { }

        public Ticket(Event @event, int seating, decimal price)
        {
            EventId = @event.Id;
            Seating = seating;
            Price = price;
        }

        public void Purchase(User user)
        {
            if(Purchased)
            {
                throw new Exception("Ticket already sold.");
            }
            UserId = user.Id;
            Username = user.Name;
            PurchasedAt = DateTime.UtcNow;
        }

        public void Cancel()
        {
            if (!Purchased)
            {
                throw new Exception("Not sold tickets can't be cancelled.");
            }
            UserId = null;
            Username = null;
            PurchasedAt = null;
        }
    }
}
