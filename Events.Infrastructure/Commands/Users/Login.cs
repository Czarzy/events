﻿using System.ComponentModel.DataAnnotations;

namespace Events.Infrastructure.Commands.Users
{
    public class Login
    {
        [Required, EmailAddress]
        public string Email { get; set; }
        [Required, MinLength(5)]
        public string Password { get; set; }
    }
}
