﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Events.Infrastructure.Commands.Users
{
    public class Register
    {
        public Guid UserId { get; set; }
        [EmailAddress]
        public string Email { get; set; }        
        [Required, MinLength(5)]
        public string Password { get; set; }
        [Required, MinLength(5)]
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
