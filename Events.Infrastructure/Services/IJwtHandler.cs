﻿using Events.Infrastructure.DTO;
using System;

namespace Events.Infrastructure.Services
{
    public interface IJwtHandler
    {
        JwtDTO CreateToken(Guid userId, string role);
    }
}
