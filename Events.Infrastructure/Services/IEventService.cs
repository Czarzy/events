﻿using Events.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Events.Infrastructure.Services
{
    public interface IEventService
    {
        Task<EventDetailsDTO> GetAsync(Guid id);

        Task<EventDetailsDTO> GetAsync(string name);

        Task<IEnumerable<EventDTO>> BrowseAsync(string name = "");

        Task CreateAsync(Guid id, string name, string description, DateTime startDate, DateTime endDate);

        Task UpdateAsync(Guid id, string name, string description);

        Task AddTicketAsync(Guid eventId, int amount, decimal price);

        Task DeleteAsync(Guid id);
    }
}
