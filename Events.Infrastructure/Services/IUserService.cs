﻿using Events.Infrastructure.DTO;
using System;
using System.Threading.Tasks;

namespace Events.Infrastructure.Services
{
    public interface IUserService
    {
        Task RegisterAsync(Guid userId, string email, string name, string password, string role = "user");

        Task<TokenDTO> LoginAsync(string email, string password);

        Task<AccountDTO> GetAccountAsync(Guid userId);
    }
}
