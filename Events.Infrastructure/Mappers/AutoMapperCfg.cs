﻿using AutoMapper;
using Events.Core.Domain;
using Events.Infrastructure.DTO;
using System.Linq;

namespace Events.Infrastructure.Mappers
{
    public static class AutoMapperCfg
    {
        public static IMapper Initialize()
        => new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Event, EventDTO>()
            .ForMember(x => x.TicketsCount, m => m.MapFrom(p => p.Tickets.Count()))
            .ForMember(x => x.AvailableTicketsCount, m => m.MapFrom(p => p.AvailableTickets.Count()))
            .ForMember(x => x.PurchasedTicketsCount, m => m.MapFrom(p => p.PurchasedTickets.Count()));
            cfg.CreateMap<Event, EventDetailsDTO>();
            cfg.CreateMap<Ticket, TicketDTO>();
            cfg.CreateMap<User, AccountDTO>();
            cfg.CreateMap<TicketDTO, TicketDetailsDTO>();
        })
        .CreateMapper();
    }
}
