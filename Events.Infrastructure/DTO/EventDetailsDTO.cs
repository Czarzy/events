﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events.Infrastructure.DTO
{
    public class EventDetailsDTO: EventDTO
    {
        public IEnumerable<TicketDTO> Tickets { get; set; }
    }
}
