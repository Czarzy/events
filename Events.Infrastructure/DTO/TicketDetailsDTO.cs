﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events.Infrastructure.DTO
{
    public class TicketDetailsDTO : TicketDTO
    {
        public Guid EventId { get; set; }

        public string EventName { get; set; }
    }
}
